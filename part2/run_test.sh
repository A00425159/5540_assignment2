#!/bin/bash
# for each phistory*csv file available it will:
#   - truncate the history table
#   - load from the file
#   - send a query
# the code is using profiling and sends the results to text files to analyze later.



for i in `seq 1 3`
do
    echo "==============="
    echo "Processing iteration $i"
    # creates a sequence to run
    for f in `ls -1 phistory_*csv`
    do
    #    echo "processing file: $f"
        N=`echo $f | cut -d _ -f 2 | cut -d . -f 1`

        # flip a coin
        # heads = 1 = create the index
        # tails = 0 = drop the index
        coin_flip=$(($(($RANDOM%10))%2))

        if [[ $coin_flip -eq 1 ]]
        then
            indexed='T'
            mysql << ! 2>/dev/null
                use assignment2;
                truncate table history;
                create index city on history(city);
!
        else
            indexed='F'
            mysql << ! 2>/dev/null
                use assignment2;
                truncate table history;
                ALTER TABLE history
                DROP INDEX city;
!
        fi

        # needs ~/.my.cnf file to connect this way
        # redirects the sql/commands to the mysql connection and gets the output piped to tail -1 to get just the last line of output (from show profile) and send to the log
        mysql << ! | tail -1 >> time_make_hist_table.log
            use assignment2;
            set profiling = 1;
            load data local infile "$f" into table history;
            show profiles;
!
        load_time=`tail -1 time_make_hist_table.log |  awk '{ print $2 }'`

        # redirects the sql/commands to the mysql connection and gets the output piped to tail -1 to get just the last line of output (from show profile) and send to the log
        mysql <<! | tail -1 >> query_runtimes.txt
            use assignment2;
            set profiling = 1;
            select "running for $N", lname, fname from person, history as H where _id=pid and eyear=2000 and H.city='Las Vegas';
            show profiles;
!
        query_time=`tail -1 query_runtimes.txt |  awk '{ print $2 }'`

        echo "`date +%Y%m%d%H%M%S`,${f},${N},${load_time},${query_time},${indexed}" | tee -a test_output.csv
    done

done

