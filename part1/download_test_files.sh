#!/bin/bash
# part1 datafiles = 'http://cs.smu.ca/~stavros/courses/current/5540_website/resources/assignment2/part1/data_files/'
# part2 http://cs.smu.ca/~stavros/courses/current/5540_website/resources/assignment2/part2/

if [ $# -ne 2 ]
then
    echo "Inform the url with the links to download and the dir to save the files to."
    exit 1
fi
url=$1
dir=$2

# downloads the page with the links to the data files
curl -u 5540:5540 $url > html

# gets the lines with links and passes through a perl filter that will extract just the file name (finds everything up to href=" and extracts what's between " " and prints
grep "^<tr>.*href.*\..*" html | perl -pe 's|.*href="(.*?\..*?)".*|\1|' > links

# iterate through the links and downloads all files
for filename in `cat links`
do
    curl -u 5540:5540 ${url}/${filename} > ${dir}/${filename}
done

rm html links
