#!/bin/bash

# reads each ".txt" file in the given folder, where the 1st line of 
# each file is the title; for each file, it makes a json object with 
# three fields: "file_name", "title", "content", where it replaces 
# any double quotes and \n's in the title/content with spaces

if [ $# -ne 2 ]
then
    printf "\nUsage: \n\$ $0 <database> <user>  \n\n"
    exit 1
fi

printf "\nMongodb password: "
read -s pass

# folder and collection are hardcoded per specification of assignment
folder="data_files"
coll="newsitems"
db="$1"
user="$2"

# if the folder does not exist, exit.
if [ ! -d $folder ]
then
	echo "Folder: $folder does not exist."
	exit 1
fi

cd $folder
files=$(ls *.txt)

printf "\nImporting from folder \"$folder\"\n-\n"
for c in $files ;
do
    echo "$c translated as json file $c.json"
    echo "$c" | python ../txt2json.py > "$c.json"
    mongoimport -d "$db" -u "$user" -p "$pass" -c "$coll" --file "$c.json"
    rm "$c.json"
done
cd ..

# add a text search index to the newsitems collection
mongo $db -u $user -p $pass --eval 'db.newsitems.createIndex( {"$**": "text"}, {weights: {file_name: 10, title: 5, content: 1}, name: "TextIndex"});'

echo

